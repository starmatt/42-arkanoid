#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/02/20 18:44:07 by jbarbie           #+#    #+#              #
#    Updated: 2015/05/03 18:02:37 by mborde           ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME		=	arkanoid

CC			=	gcc

CFLAGS		+=	-Wall -Wextra -Werror

SRC			=	$(shell ls -1 src)

INC			=	-I inc/ -I glfw/include/ -I libft/inc

OBJ			=	$(patsubst %.c, obj/%.o, $(SRC))

LIBFT		=	libft/libft.a

GLFW		=	glfw/src/libglfw3.a

LIB			=	-Lglfw/src -lglfw3 -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo\

ifeq ($(BUILD),d)
    CFLAGS += -g
endif
ifeq ($(BUILD),o)
    CFLAGS += -03
endif

OS = $(shell uname -s)
ECHO = echo
ifeq ($(OS),Linux)
    ECHO += -e
endif

RM			=	rm -Rf

all			:	$(NAME)

$(NAME)		:	$(GLFW) obj $(LIBFT) $(OBJ)
				@$(CC) $(CFLAGS) -o $@ $(OBJ) $(LIBFT) $(LIB)

obj/%.o		:	src/%.c
				@gcc $(CFLAGS) $(INC) -o $@ -c $<
				@$(ECHO) "[\033[1;32m√\033[m]" $<

obj			:
				@mkdir -p obj

$(LIBFT)	:
				@make -C libft/

$(GLFW)		:
				@git submodule init
				@git submodule update
				@cd glfw && cmake .&& make

clean		:
				@$(ECHO) "\033[33;31mCleaning files .o ...\033[0m"
				@$(RM) obj/
				@make -C libft/ clean

fclean		:	clean
				@$(ECHO) "\033[33;31mCleaning executables...\033[0m"
				@$(RM) $(NAME)
				@$(RM) $(LIBFT)

norme		:
				norminette $(SRC) $(INC)

re			:	fclean all
