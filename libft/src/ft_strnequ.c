/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 09:07:14 by jbarbie           #+#    #+#             */
/*   Updated: 2014/11/10 09:21:39 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	n_char(const char *s, size_t n)
{
	size_t	i;
	int		ret;

	i = 0;
	ret = 0;
	if (n)
	{
		while (s[i] && i < n)
		{
			ret = ret + (int)s[i];
			i++;
		}
		return (ret);
	}
	return (1);
}

int			ft_strnequ(char const *s1, char const *s2, size_t n)
{
	int	str1;
	int	str2;

	if (n == 0)
		return (1);
	if (!s1 || !s2)
		return (0);
	str1 = n_char(s1, n);
	str2 = n_char(s2, n);
	if (str1 < str2 || str1 > str2)
		return (0);
	else
	{
		if (s1[0] > s2[0] || s1[0] < s2[0])
			return (0);
		return (1);
	}
}
