/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 09:07:14 by jbarbie           #+#    #+#             */
/*   Updated: 2014/11/11 12:01:55 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	s_char(const char *s)
{
	int	i;
	int	ret;

	i = 0;
	ret = 0;
	while (s[i])
	{
		ret = ret + (int)s[i];
		i++;
	}
	return (ret);
}

int			ft_strequ(char const *s1, char const *s2)
{
	int	str1;
	int	str2;

	if (!s1)
		return (0);
	if (!s2)
		return (0);
	str1 = s_char(s1);
	str2 = s_char(s2);
	if (str1 < str2)
		return (0);
	else if (str1 > str2)
		return (0);
	else
	{
		if (s1[0] > s2[0])
			return (0);
		if (s1[0] < s2[0])
			return (0);
		return (1);
	}
}
