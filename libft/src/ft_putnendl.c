/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnendl.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/29 04:55:14 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/31 12:53:08 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void ft_putnendl(char *str, size_t n)
{
	size_t i;

	i = 0;
	if (n > ft_strlen(str))
		ft_putendl(str);
	while (i < n)
	{
		ft_putchar(str[i]);
		i++;
	}
	ft_putchar('\n');
}
