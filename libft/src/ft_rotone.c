/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotone.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/11 23:03:33 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/29 05:53:42 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_rotone(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (ft_isupper(str[i]) == 1)
		{
			if (str[i] != 'Z')
				str[i] += 1;
			else
				str[i] = 'A';
		}
		else if (ft_islower(str[i]) == 1)
		{
			if (str[i] != 'z')
				str[i] += 1;
			else
				str[i] = 'a';
		}
		i++;
	}
	return (str);
}
