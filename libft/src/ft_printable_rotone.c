/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printable_rotone.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/29 03:22:06 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/29 05:52:23 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char *ft_printable_rotone(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if ((ft_isprint(str[i]) == 1) && str[i] != '~')
			str[i] += 1;
		else if (str[i] == 126)
			str[i] = 32;
		i++;
	}
	return (str);
}
