/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strupcase.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/29 05:16:41 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/31 12:50:02 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char *ft_strupcase(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (ft_islower(str[i]) == 1)
			ft_toupper(str[i]);
		i++;
	}
	return (str);
}
