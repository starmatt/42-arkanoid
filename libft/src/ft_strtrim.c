/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 14:45:38 by jbarbie           #+#    #+#             */
/*   Updated: 2014/11/10 11:03:24 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_check_char(char const *s)
{
	size_t	i;

	i = 0;
	while (*s)
	{
		if (*s != '\n' && *s != ' ' && *s != '\t')
			i++;
		s++;
	}
	if (i == 0)
		return (0);
	return (1);
}

char			*ft_strtrim(char const *s)
{
	size_t	i;
	size_t	y;
	size_t	space;

	i = 0;
	y = 0;
	space = 0;
	if (!s)
		return (NULL);
	if ((ft_check_char(s) == 0))
		return (ft_memalloc(1));
	y = ft_strlen(s) - 1;
	while (s[i] == '\n' || s[i] == ' ' || s[i] == '\t')
	{
		i++;
		space++;
	}
	while (s[y] == '\n' || s[y] == ' ' || s[y] == '\t')
	{
		y--;
		space++;
	}
	return (ft_strsub(s, i, ft_strlen(s) - space));
}
