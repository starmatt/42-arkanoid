/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrijoin.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/12 09:35:36 by jbarbie           #+#    #+#             */
/*   Updated: 2015/01/12 09:40:07 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strtrijoin(char *s1, char *s2, char *s3)
{
	char	*tmp;
	char	*join;

	tmp = ft_strjoin(s1, s2);
	join = ft_strjoin(tmp, s3);
	free (tmp);
	return (join);
}
