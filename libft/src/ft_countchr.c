/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_countchr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/13 15:44:50 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/27 13:28:35 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_countchr(char *str, char c)
{
	int i;
	int chr;

	i = 0;
	chr = 0;
	while (str[i])
	{
		if (str[i] == c)
		{
			chr++;
			i++;
		}
		else
			i++;
	}
	return (chr);
}
