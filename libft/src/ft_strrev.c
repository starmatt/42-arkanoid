/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/29 05:26:39 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/29 05:27:31 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrev(char *str)
{
	int		a;
	int		b;
	int		len;
	char	tmp;

	b = 0;
	len = ft_strlen(str);
	a = 0;
	b = len - 1;
	while (a < len / 2)
	{
		tmp = str[a];
		str[a] = str[b];
		str[b] = tmp;
		a++;
		b--;
	}
	return (str);
}
