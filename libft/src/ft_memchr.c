/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 10:52:34 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/28 20:03:54 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned int	i;
	unsigned char	*ret;

	i = 0;
	ret = (unsigned char*)s;
	if (s && n > 0)
	{
		while (i < n)
		{
			if (ret[i] == (unsigned char)c)
				return ((void*)(ret + i));
			i++;
		}
	}
	return (NULL);
}
