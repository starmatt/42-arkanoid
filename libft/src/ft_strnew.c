/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 19:01:17 by jbarbie           #+#    #+#             */
/*   Updated: 2015/05/02 05:46:55 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char *ft_strnew(size_t size)
{
	char	*str;

	if ((str = malloc(sizeof(size_t) * size + 1)) && size < SIZE_MAX)
	{
		ft_bzero(str, size);
		return (str);
	}
	return (NULL);
}
