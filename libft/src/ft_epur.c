/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_epur.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/01 15:23:45 by jbarbie           #+#    #+#             */
/*   Updated: 2015/02/01 16:31:31 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char *ft_epur(char *str)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (str[i])
	{
		while (str[i] != ' ' && str[i] != '\t' && str[i])
		{
			str[j] = str[i];
			i++;
			j++;
		}
		if ((str[i] == ' ' || str[i] == '\t') && str[i])
		{
			str[j] = ' ';
			i++;
			j++;
			while ((str[i] == ' ' || str[i] == '\t') && str[i])
				i++;
		}
	}
	str[j] = '\0';
	return (str);
}
