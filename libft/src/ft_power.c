/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_power.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/29 00:12:12 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/31 12:43:14 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int ft_power(int nb, int power)
{
	int ret;

	ret = nb;
	if (power < 0 || power == 0 || nb > 2147483647)
		return (0);
	while (power > 1)
	{
		ret *= nb;
		power--;
	}
	return (ret);
}
