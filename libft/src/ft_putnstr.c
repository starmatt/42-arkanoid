/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/29 04:55:14 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/31 12:55:38 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void ft_putnstr(char *str, size_t n)
{
	size_t	i;

	i = 0;
	if (n > ft_strlen(str))
		ft_putstr(str);
	while (i < n)
	{
		ft_putchar(str[i]);
		i++;
	}
}
