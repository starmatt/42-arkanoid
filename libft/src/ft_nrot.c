/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nrot.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/29 00:46:03 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/29 03:19:37 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	nrotate(char *str, int nb)
{
	nb = nb % 26;
	while (*str)
	{
		if (ft_isalpha(*str) == 1 && ft_isupper(*str) == 1)
		{
			if (ft_isupper(*str + nb) == 1)
				ft_putchar(*str + nb);
			else
				ft_putchar(*str - 26 + nb);
		}
		else if (ft_isalpha(*str) == 1)
		{
			if (ft_isalpha(*str + nb) == 1)
				ft_putchar(*str + nb);
			else
				ft_putchar(*str - 26 + nb);
		}
		else
			ft_putchar(*str);
		++str;
	}
	ft_putchar('\n');
}
