/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bubsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/20 15:14:27 by jbarbie           #+#    #+#             */
/*   Updated: 2015/02/01 15:12:54 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

void		ft_bubsort(int *tab)
{
	int i;
	int n;
	int swap;

	n = sizeof(tab);
	swap = 1;
	ft_putchar('\n');
	ft_putnbr(n);
	while (swap == 1)
	{
		i = 0;
		while (i < n)
		{
			if (tab[i] > tab[i + 1])
			{
				ft_swap_int(tab[i], tab[i + 1]);
				swap = 1;
			}
			else
				swap = 0;
			i++;
		}
	}
}
