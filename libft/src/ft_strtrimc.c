/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrimc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 14:45:38 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/31 12:51:25 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_check_char(char const *s, char c)
{
	size_t	i;

	i = 0;
	while (*s)
	{
		if (*s != c)
			i++;
		s++;
	}
	if (i == 0)
		return (0);
	return (1);
}

char			*ft_strtrimc(char const *s, char c)
{
	size_t	i;
	size_t	y;
	size_t	space;

	i = 0;
	y = 0;
	space = 0;
	if (!s)
		return (NULL);
	if ((ft_check_char(s, c) == 0))
		return (ft_memalloc(1));
	y = ft_strlen(s) - 1;
	while (s[i] == c)
	{
		i++;
		space++;
	}
	while (s[y] == c)
	{
		y--;
		space++;
	}
	return (ft_strsub(s, i, ft_strlen(s) - space));
}
