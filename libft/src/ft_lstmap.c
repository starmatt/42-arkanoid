/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/27 15:30:59 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/28 20:17:54 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *new;
	t_list *ret;
	t_list *tmp;

	if (!lst || !f)
		return (NULL);
	if (!(new = f(lst)))
		return (NULL);
	tmp = new;
	ret = new;
	lst = lst->next;
	while (lst)
	{
		if (!(new = f(lst)))
			return (NULL);
		tmp->next = new;
		tmp = new;
		lst = lst->next;
	}
	new->next = NULL;
	return (ret);
}
