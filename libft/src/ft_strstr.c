/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 13:07:56 by jbarbie           #+#    #+#             */
/*   Updated: 2014/12/20 10:49:18 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	int i;
	int pos;
	int len;

	i = 0;
	pos = 0;
	len = 0;
	if (s2[len] == '\0')
		return ((char *)s1);
	while (s1[i])
	{
		while (s1[pos + i] == s2[pos])
		{
			pos++;
			if (s2[pos] == '\0')
				return ((char *)&s1[i]);
		}
		pos = 0;
		i++;
	}
	return (NULL);
}
