/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 09:03:55 by jbarbie           #+#    #+#             */
/*   Updated: 2014/11/08 15:43:01 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char *ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int		i;
	char	*tmp;

	i = 0;
	if (s && f)
	{
		if ((tmp = (char *)malloc(sizeof(char) * (ft_strlen(s) + 1))))
		{
			while (s[i])
			{
				tmp[i] = f(i, s[i]);
				i++;
			}
			tmp[i] = '\0';
			return (tmp);
		}
		return (NULL);
	}
	return (NULL);
}
