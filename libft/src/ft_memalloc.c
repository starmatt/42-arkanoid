/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 18:08:22 by jbarbie           #+#    #+#             */
/*   Updated: 2014/11/08 10:37:18 by jbarbie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void *ft_memalloc(size_t size)
{
	void	*mem;
	size_t	i;
	char	*str;

	i = 0;
	if (!(mem = malloc(size)))
		return (NULL);
	str = (char *)mem;
	while (i < size)
	{
		*str = 0;
		i++;
		str++;
	}
	return (mem);
}
