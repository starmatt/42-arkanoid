/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arkanoid.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/01 23:06:39 by jbarbie           #+#    #+#             */
/*   Updated: 2015/05/03 23:18:01 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARKANOID_H
# define ARKANOID_H

# include <unistd.h>
# include <stdlib.h>
# include "libft.h"
# include <GLFW/glfw3.h>
# include <GLUT/glut.h>
# include <fcntl.h>

# define TRUE 1
# define FALSE 0

typedef struct	s_game
{
	GLFWwindow	*win;
	int			win_x;
	int			win_y;
	int			getlvl;
	char		*lvl;
	char		lvl_next;
	int			len;
	char		**level;
	int			ball_x;
	int			ball_y;
	int			directx;
	int			directy;
	int			lives;
	int			lines;
	float		v1x;
	float		v1y;
	float		v2x;
	float		v2y;
	float		v3x;
	float		v3y;
	float		v4x;
	float		v4y;
}				t_game;

GLfloat			g_x;
int				g_mod;
int				g_launch;
int				get_level(t_game *g);
int				game(t_game *g);
void			init_ball(t_game *g);
void			draw_ball(t_game *g);
void			check_brick(t_game *g);
void			move_left(void);
void			move_right(void);
int				ft_puterror(char *s);
void			save_pos(int x, int y, float size, t_game *g);
void			next_level(t_game *g);

#endif
