/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/02 00:20:50 by mborde            #+#    #+#             */
/*   Updated: 2015/05/03 23:40:23 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

void		save_pos(int x, int y, float size, t_game *g)
{
	g->v1x = -1.0f + ((float)x * size / (g->win_x / 2)) + 0.005f;
	g->v1y = 0.99f - (0.10 * y);
	g->v2x = -1.0f + ((float)(x + 1) * size / (g->win_x / 2)) - 0.005f;
	g->v2y = 0.99f - (0.10 * y);
	g->v3x = -1.0f + ((float)(x + 1) * size / (g->win_x / 2)) - 0.005f;
	g->v3y = 0.90f - (0.10 * y);
	g->v4x = -1.0f + ((float)x * size / (g->win_x / 2)) + 0.005f;
	g->v4y = 0.90f - (0.10 * y);
}

static void	free_level(t_game *g)
{
	int	i;

	i = 0;
	while (g->level[i])
	{
		free(g->level[i]);
		i++;
	}
}

static void	victory(t_game *g)
{
	free_level(g);
	ft_putstr("Victory is yours!");
	exit(0);
}

void		next_level(t_game *g)
{
	int	x;
	int	y;

	y = 0;
	if (g->lvl[0] > '0' && g->lvl[0] <= '3')
	{
		while (g->level[y])
		{
			x = 0;
			while (g->level[y][x])
			{
				if (g->level[y][x] >= '0' && g->level[y][x] <= '2')
					return ;
				x++;
			}
			y++;
		}
		g->lvl[0]++;
		free_level(g);
	}
	else if (g->lvl[0] > '3')
		victory(g);
}

int			ft_puterror(char *s)
{
	ft_putendl_fd(s, 2);
	return (-1);
}
