/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ball.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 11:50:40 by jbarbie           #+#    #+#             */
/*   Updated: 2015/05/03 23:17:47 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

static void	check_pad(t_game *g)
{
	if (g->ball_y == 0)
	{
		if (((float)g->ball_x * 0.01) < g_x + 0.12 &&
			((float)g->ball_x * 0.01) > g_x - 0.12)
			g->directy = 1;
		else
		{
			g->lives--;
			if (g->lives == 0)
			{
				ft_putendl("You lost freak.");
				exit(0);
			}
			g_launch = FALSE;
		}
	}
	check_brick(g);
}

static void put_vertex(t_game *g)
{
	(void)g;
	glBegin(GL_QUADS);
	glColor3f(1, 1, 1);
	glVertex2f(0.0f + ((float)g->ball_x * 0.01f),
	-0.87f + ((float)g->ball_y * 0.01f));
	glVertex2f(0.0f + ((float)g->ball_x * 0.01f),
	-0.9f + ((float)g->ball_y * 0.01f));
	glVertex2f(0.028f + ((float)g->ball_x * 0.01f),
	-0.9f + ((float)g->ball_y * 0.01f));
	glVertex2f(0.028f + ((float)g->ball_x * 0.01f),
	-0.87f + ((float)g->ball_y * 0.01f));
	glEnd();
}

void		draw_ball(t_game *g)
{
	if (g->ball_y < 185 && g->directy == 1)
	{
		if (g->ball_y + 1 == 185)
			g->directy = -1;
		g->ball_y++;
	}
	else if (g->ball_y > 0 && g->directy == -1)
	{
		g->directy = -1;
		g->ball_y--;
	}
	if (g->ball_x < 97 && g->directx == 1)
	{
		if (g->ball_x + 1 == 97)
			g->directx = -1;
		g->ball_x++;
	}
	else if (g->ball_x > -99 && g->directx == -1)
	{
		if (g->ball_x - 1 == -99)
			g->directx = 1;
		g->ball_x--;
	}
	put_vertex(g);
	check_pad(g);
}

void		init_ball(t_game *g)
{
	g->directx = 1;
	g->directy = 1;
	g->ball_x = g_x * 100;
	g->ball_y = 0;
}
