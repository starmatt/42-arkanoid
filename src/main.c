/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbarbie <jbarbie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/01 21:18:23 by jbarbie           #+#    #+#             */
/*   Updated: 2015/05/09 18:07:53 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

static void	key_callback(GLFWwindow *window, int key, int scancode,
	int action, int mods)
{
	(void)scancode;
	(void)mods;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	if (key == GLFW_KEY_A && (action == GLFW_PRESS || action == GLFW_REPEAT))
		move_left();
	if (key == GLFW_KEY_D && (action == GLFW_PRESS || action == GLFW_REPEAT))
		move_right();
	if (key == GLFW_KEY_SPACE && (action == GLFW_PRESS ||
		action == GLFW_REPEAT))
		g_launch = TRUE;
}

int			main(void)
{
	t_game	g;

	g_x = 0;
	g_mod = 7;
	g.lives = 5;
	g.win_x = 800;
	g.win_y = 600;
	g.getlvl = FALSE;
	g.lvl = ft_strdup("1");
	g.lvl_next = '1';
	g_launch = FALSE;
	glfwInit();
	g.win = glfwCreateWindow(g.win_x, g.win_y, "Arkanoid", NULL, NULL);
	glfwMakeContextCurrent(g.win);
	glfwSwapInterval(1);
	glfwSetKeyCallback(g.win, key_callback);
	while (!glfwWindowShouldClose(g.win))
		game(&g);
	glfwDestroyWindow(g.win);
	return (0);
}
