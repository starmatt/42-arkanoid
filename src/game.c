/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/02 02:09:46 by mborde            #+#    #+#             */
/*   Updated: 2015/05/03 23:40:33 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

static void	put_vertex(int x, int y, int value, t_game *g)
{
	float	size;

	glBegin(GL_QUADS);
	size = ((float)g->win_x / g->len);
	save_pos(x, y, size, g);
	glColor3f(1, 1 - 0.1 * value, 1 - 0.1 * value);
	glVertex2f(g->v1x, g->v1y);
	glColor3f(0.3f, 0.3f - 0.1 * value, 0.3f - 0.1 * value);
	glVertex2f(g->v2x, g->v2y);
	glVertex2f(g->v3x, g->v3y);
	glVertex2f(g->v4x, g->v4y);
	glEnd();
}

static int	draw_level(t_game *g)
{
	int		x;
	int		y;
	int		value;

	if (g->lvl[0] == g->lvl_next)
	{
		get_level(g);
		init_ball(g);
	}
	if (g->level && g->level[0])
		g->len = ft_strlen(g->level[0]);
	y = 0;
	while (g->level && g->level[y])
	{
		x = 0;
		while (g->level[y][x])
		{
			value = g->level[y][x] - '0';
			if (g->level[y][x] >= '0')
				put_vertex(x, y, value, g);
			x++;
		}
		y++;
	}
	return (0);
}

static int	draw_player(void)
{
	glBegin(GL_QUADS);
	glColor3f(153, 255, 204);
	glVertex2f(-0.1f + g_x, 0.01f - 0.9f);
	glVertex2f(0.1f + g_x, 0.01f - 0.9f);
	glVertex2f(0.1f + g_x, -0.01f - 0.9f);
	glVertex2f(-0.1f + g_x, -0.01f - 0.9f);
	glEnd();
	return (0);
}

int			game(t_game *g)
{
	if (g->win_x <= 200 && g->win_y <= 200)
		glfwSetWindowSize(g->win, 800, 600);
	glfwGetFramebufferSize(g->win, &(g->win_x), &(g->win_y));
	glViewport(0, 0, g->win_x, g->win_y);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	draw_player();
	draw_level(g);
	if (g_launch == TRUE)
		draw_ball(g);
	else if (g_launch == FALSE)
		init_ball(g);
	glfwSwapBuffers(g->win);
	glfwPollEvents();
	return (0);
}
