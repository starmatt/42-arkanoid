/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_level.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/01 23:47:45 by mborde            #+#    #+#             */
/*   Updated: 2015/05/03 23:21:41 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

static int	line_size(int fd)
{
	char	*line;
	int		i;

	i = 0;
	while (ft_gnl(fd, &line) == 1)
	{
		i++;
		free(line);
	}
	return (i);
}

static int	make_level(int fd, char *lvln, t_game *g)
{
	int		x;
	int		y;
	char	*line;

	y = line_size(fd);
	close (fd);
	if (!(g->level = (char **)malloc(sizeof(char *) * y + 1)))
		return (-1);
	if ((fd = open(lvln, O_RDWR)) != -1)
	{
		x = 0;
		while (ft_gnl(fd, &line) > 0)
		{
			g->level[x] = ft_strdup(line);
			x++;
			free(line);
		}
		g->level[x] = NULL;
		g->lvl_next++;
	}
	return (0);
}

int			get_level(t_game *g)
{
	int		fd;
	char	*lvln;

	lvln = ft_strjoin("levels/level", g->lvl);
	if ((fd = open(lvln, O_RDWR)) != -1)
		make_level(fd, lvln, g);
	else
		return (-1);
	close(fd);
	free(lvln);
	return (0);
}
