/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   brick_hit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 15:58:24 by mborde            #+#    #+#             */
/*   Updated: 2015/05/03 22:59:14 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"
#include <stdio.h>

static int	the_return_ofcheck_brick(t_game *g, int x, int y, float size)
{
	if (g->ball_y <= 185 && g->ball_y >= 180 - (y * 10))
	{
		if ((float)g->ball_x * 0.01 >= -1.0 + ((float)x * size /
					(g->win_x / 2)) + 0.005 &&
				(float)g->ball_x * 0.01 <= -1.0 + ((float)(x + 1) *
					size / (g->win_x / 2)) - 0.005)
		{
			g->directy *= -1;
			if (g->level[y][x] == '0')
				g->level[y][x] = ' ';
			else if (g->level[y][x] == '1')
				g->level[y][x] = '0';
			else if (g->level[y][x] == '2')
				g->level[y][x] = '1';
			return (1);
		}
	}
	return (0);
}

void		check_brick(t_game *g)
{
	int		x;
	int		y;
	float	size;

	y = 0;
	if (g->level && g->level[0])
		size = ((float)g->win_x / g->len);
	while (g->level[y])
	{
		x = 0;
		while (g->level[y][x])
		{
			if (g->level[y][x] != ' ')
			{
				if (the_return_ofcheck_brick(g, x, y, size) == 1)
				{
					next_level(g);
					return ;
				}
			}
			x++;
		}
		y++;
	}
}
